GuiltySpark
===========

An easily extensible discord bot.
---------------------------------

Guilty spark is a bare bones discord bot focused on extensibility. /Almost/ all of the functionality provided is
contained in a plugin.

No more will you need to rewrite bot after bot to get the features you need. With the powerful plugin system you can
access all features and functions of the bot.

Required libraries
------------------
- discord python api
- PyYaml
- aioredis
